package com.steedles.steedchat.helpers

open class User(
    var id: String? = "",
    var email: String? = "",
    var displayName: String? = "",
    var chatList: MutableList<String> = arrayListOf()
)

open class Chat(
    var id: String? = "",
    var user1: String? = "",
    var user2: String? = "",
    var mostRecentMessage: String? = ""
)

open class RecentChat(
    var chatId: String? = "",
    var displayName: String? = "",
    var toUserId: String? = "",
    var recentMessage: String? = null

)

open class ChatMessage(
    var id: String? = "",
    var message: String? = "",
    var messageType: String = MessageTypes.TEXT,
    var chatId: String? = "",
    var userFrom: String? = "",
    var userTo: String? = "",
    var messageDateTime: String? = "",
    var messageRead: Boolean = false
)

object MessageTypes {
    const val TEXT = "text"
    const val IMAGE = "image"
}


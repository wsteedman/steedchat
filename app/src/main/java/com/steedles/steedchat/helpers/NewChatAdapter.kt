package com.steedles.steedchat.helpers

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.steedles.steedchat.R
import kotlinx.android.synthetic.main.new_chat_item.view.*

class NewChatAdapter(val items: ArrayList<User>, val context: Context, val onUserListener: OnUserListener) :
    RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.new_chat_item, parent, false), onUserListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.newChat.text = items.get(position).displayName
    }

    interface OnUserListener {
        fun onUserClick(position: Int)
    }
}

class ViewHolder (view: View, private val onUserListener: NewChatAdapter.OnUserListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
    val newChat = view.new_chat_item!!

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onUserListener.onUserClick(adapterPosition)
    }
}
package com.steedles.steedchat.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.steedles.steedchat.R
import kotlinx.android.synthetic.main.recent_chat_item.view.*

class RecentChatAdapter(val items: ArrayList<RecentChat>, val context: Context, val onChatListener: OnChatListener, val userId: String) :
    RecyclerView.Adapter<RecentChatViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentChatViewHolder {
        return RecentChatViewHolder(LayoutInflater.from(context).inflate(R.layout.recent_chat_item, parent, false), onChatListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecentChatViewHolder, position: Int) {
        val item = items.get(position)
        holder.recentChatName.text = item.displayName
        holder.recentChatMessage.text = item.recentMessage
    }

    interface OnChatListener {
        fun onChatClick(position: Int)
    }
}

class RecentChatViewHolder (view: View, private val onChatListener: RecentChatAdapter.OnChatListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
    val recentChatName = view.recent_chat_item!!
    val recentChatMessage = view.recent_message!!

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        onChatListener.onChatClick(adapterPosition)
    }
}
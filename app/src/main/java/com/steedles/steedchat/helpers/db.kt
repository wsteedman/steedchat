package com.steedles.steedchat.helpers

import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.tasks.await
import java.util.*
import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty

open class Database() {

    companion object {
        private const val TAG = "Database"
    }

    private var db : FirebaseFirestore = FirebaseFirestore.getInstance()
    private var auth : FirebaseAuth = FirebaseAuth.getInstance()
    lateinit var user: User
    lateinit var users: List<User>

    fun createUser(userId: String, email: String, displayName: String) {
        val user = User(userId, email, displayName)
        this.user = user
        db.collection("users").document(userId).set(user)
    }

    fun loadUserTrack(userId: String): DocumentReference {
        val docRef = db.collection("users").document(userId)
        return docRef
    }

    suspend fun loadUser(userId: String): User? {
        val user = db.collection("users").document(userId).get().await()
        return user.toObject(User::class.java)
    }

    suspend fun loadMessage(messageId: String): ChatMessage? {
        Log.d(TAG, "loading user")
        val message = db.collection("chat_messages").document(messageId).get().await()
        return message.toObject(ChatMessage::class.java)
    }

    fun loadUserChats(user: User): Query {
        val docRef = db.collection("user_chats").whereIn("id", user.chatList)
        return docRef
    }

    fun createChatIfNotExist(userId: String): String {
        val sourceId = auth.currentUser?.uid
        val chatId = getChatId(sourceId!!, userId)

        val chat = Chat(chatId, sourceId, userId, "")

        db.collection("user_chats").document(chatId).set(chat)

        addToChatList(sourceId, chatId)
        addToChatList(userId, chatId)

        return chatId
    }

    private fun addToChatList(userId: String, chatId: String) {
        db.collection("users").document(userId).get().addOnSuccessListener { documentSnapshot ->
            val user = documentSnapshot.toObject(User::class.java)
            if (!user?.chatList!!.contains(chatId)) {
                user.chatList.add(chatId)
                db.collection("users").document(userId).update("chatList", user.chatList)
            }
        }
    }

    fun loadAllUsers(): Task<QuerySnapshot> {
        Log.d(TAG, "loading all users")
        val docRef = db.collection("users")

        return docRef.get()
    }

    fun loadChatMessages(chatId: String) : Query {
        return db.collection("chat_messages").whereEqualTo("chatId", chatId)
    }

    fun getChatId(userId1: String, userId2: String): String {
        val users = listOf(userId1,userId2).sorted()
        return users.joinToString("|")
    }

    fun sendMessage(message: String, senderId: String, receiverId: String, messageType: String = MessageTypes.TEXT) {
        val chatId = getChatId(senderId, receiverId)
        val time = (System.currentTimeMillis() / 1000).toString()
        val id = "$time|$senderId|$receiverId"
        val newMessage = ChatMessage(id, message, messageType, chatId, senderId, receiverId, time, false)

        db.collection("chat_messages").document(id).set(newMessage)

        if (messageType == MessageTypes.IMAGE) {
            db.collection("user_chats").document(chatId).update("mostRecentMessage", "Sent Image...")
        } else {
            db.collection("user_chats").document(chatId).update("mostRecentMessage", message)
        }
    }
}
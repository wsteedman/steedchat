package com.steedles.steedchat.helpers

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.steedles.steedchat.R
import kotlinx.android.synthetic.main.image_message_item.view.*
import kotlinx.android.synthetic.main.text_message_item.view.*
import kotlin.math.roundToInt


class ChatMessagesAdapter(val items: ArrayList<ChatMessage>, val context: Context, val userId: String) :
    RecyclerView.Adapter<ChatMessagesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessagesViewHolder {
        return when (viewType) {
            1 -> {
                ChatMessagesViewHolder(LayoutInflater.from(context).inflate(R.layout.text_message_item, parent, false))
            }
            2 -> {
                ChatMessagesViewHolder(LayoutInflater.from(context).inflate(R.layout.image_message_item, parent, false))
            }
            else -> {
                ChatMessagesViewHolder(LayoutInflater.from(context).inflate(R.layout.text_message_item, parent, false))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].messageType) {
            MessageTypes.TEXT -> {
                1
            }
            MessageTypes.IMAGE -> {
                2
            }
            else -> {
                1
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ChatMessagesViewHolder, position: Int) {
        val item = items.get(position)
        when (item.messageType) {
            MessageTypes.IMAGE -> {
                val targetImage = holder.itemView.image_message
                if (userId == item.userFrom) {
                    val rlp = RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                    )
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                    targetImage.layoutParams = rlp
                }
                val displayMetrics = DisplayMetrics()
                val activity = context as Activity
                activity.windowManager.defaultDisplay.getMetrics(displayMetrics)

                val dimension = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels)/2.1
                Picasso.get().load(item.message).resize(dimension.roundToInt(), dimension.roundToInt()).into(targetImage)
            }
            else -> {

                if (userId == item.userFrom) {
                    val rlp = RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                    )
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                    rlp.leftMargin = 100
                    rlp.rightMargin = 15
                    rlp.topMargin = 15
                    rlp.bottomMargin = 15
                    holder.itemView.message_item.layoutParams = rlp
                }
                holder.itemView.message_item.text = item.message
                if (userId == item.userFrom) holder.itemView.message_item.textAlignment = View.TEXT_ALIGNMENT_VIEW_END
            }
        }

    }
}

class ChatMessagesViewHolder (view: View) : RecyclerView.ViewHolder(view)
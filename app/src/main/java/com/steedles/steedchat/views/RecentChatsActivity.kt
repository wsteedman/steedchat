package com.steedles.steedchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.steedles.steedchat.helpers.*
import com.steedles.steedchat.views.ChatMessagesActivity
import com.steedles.steedchat.views.NewChatActivity
import kotlinx.android.synthetic.main.activity_chat_messages.*
import kotlinx.android.synthetic.main.activity_recent_chat.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

class RecentChatsActivity : AppCompatActivity(), RecentChatAdapter.OnChatListener {

    companion object {
        private const val TAG = "ChatList"
    }

    private lateinit var auth: FirebaseAuth
    private var userId: String = ""
    private val db: Database = Database()
    private var recentChatList: List<RecentChat> by Delegates.observable(mutableListOf()) { _, old, new ->
        Log.d(TAG, "new $new")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recent_chat)
        supportActionBar?.title = "Your Chats"
        if (!checkUser()) return

        recyclerview_recent_chats.adapter = RecentChatAdapter(ArrayList(recentChatList), this, this, userId)

        db.loadUserTrack(userId).addSnapshotListener { snapshot, e ->
            val user = snapshot!!.toObject(User::class.java)
            if (user!!.chatList.isNotEmpty()) {
                db.loadUserChats(user)
                    .addSnapshotListener { snapshot, e ->
                        val chatList = snapshot?.toObjects(Chat::class.java)!!
                        CoroutineScope(IO).launch {
                            getRecentChatList(chatList)
                        }

                    }
            }
        }
    }

    private suspend fun getRecentChatList(chatList: List<Chat>) {

        val newChatList = mutableListOf<RecentChat>()
        chatList.forEach {
            var displayUser = it.user1
            var toUserId = it.user1
            if (it.user1 == userId) {
                displayUser = it.user2
                toUserId = it.user2
            }
            val recentChatUser = db.loadUser(displayUser!!)
            var recentChatMessage = ""
            if (it.mostRecentMessage != null && it.mostRecentMessage!!.isNotEmpty()) {
                recentChatMessage = it.mostRecentMessage!!.lines()[0]
                if (recentChatMessage.length > 23) recentChatMessage = "${recentChatMessage.substring(IntRange(0,20))}..."
            }

            val recentChat = RecentChat(it.id, recentChatUser?.displayName, toUserId,
                recentChatMessage
            )

            newChatList.add(recentChat)
        }

        Log.d(TAG, "newChatList ${newChatList.size}")
        recentChatList = newChatList

        runOnUiThread(Runnable {
            if (recentChatList.isNotEmpty()) empty_view.visibility = View.GONE
            recyclerview_recent_chats.adapter = RecentChatAdapter(ArrayList(recentChatList), this, this, userId)
        })
    }

    private fun checkUser(): Boolean {
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        if (user == null) {
            val intent = Intent(this, RegisterActivity::class.java)
            finish()
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            return false
        }

        userId = user.uid
        return true
    }

    private fun signOut() {
        auth.signOut()
        val intent = Intent(this, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_logout -> {
                this.signOut()
            }
            R.id.menu_new_chat -> {
                val intent = Intent(this, NewChatActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onChatClick(position: Int) {
        val chat = recentChatList[position]
        val uid = chat.toUserId

        val intent = Intent(this, ChatMessagesActivity::class.java)
        intent.putExtra("chatId", uid)

        startActivity(intent)
    }
}

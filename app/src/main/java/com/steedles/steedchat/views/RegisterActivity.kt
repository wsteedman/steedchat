package com.steedles.steedchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.steedles.steedchat.helpers.Database
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "Register"
        private const val EMAIL_REGEX = ".*"
        private const val PASS_REGEX = ".*"
    }

    private lateinit var auth: FirebaseAuth
    private val db: Database = Database()

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser != null) {
            val intent = Intent(this, RecentChatsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        button_register_register.setOnClickListener {
            val displayName = edittext_register_displayname.text.toString()
            val email = edittext_register_email.text.toString()
            val password = edittext_register_password.text.toString()
            val confirm = edittext_register_confirm.text.toString()

            registerWithEmailAndPassword(displayName, email, password, confirm)
        }

        button_register_already.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }

    private fun registerWithEmailAndPassword(displayName: String, email: String, password: String, confirm: String) {
        if (!EMAIL_REGEX.toRegex().matches(email)) {
            Log.d(TAG, "Email Failed")
            return
        }
        if (!PASS_REGEX.toRegex().matches(password)) {
            Log.d(TAG, "Password Failed")
            return
        }
        if (password != confirm) {
            Log.d(TAG, "Confirm Failed")
            return
        }

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = FirebaseAuth.getInstance().currentUser
                    if (user != null) {
                        db.createUser(user.uid, email, displayName)
                    }

                    val intent = Intent(this, RecentChatsActivity::class.java)
                    startActivity(intent)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }


    }
}

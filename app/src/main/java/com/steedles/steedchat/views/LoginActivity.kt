package com.steedles.steedchat

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.steedles.steedchat.helpers.Database
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "Login"
    }

    private lateinit var auth: FirebaseAuth
    private val db: Database = Database()

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (currentUser != null) {
            val intent = Intent(this, RecentChatsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        button_login_register.setOnClickListener {
            finish()
        }

        button_login_login.setOnClickListener {
            val email = edittext_login_email.text.toString()
            val password = edittext_login_password.text.toString()
            Log.d(TAG, "Logging in...")

            loginWithEmailAndPassword(email, password)
        }
    }

    private fun loginWithEmailAndPassword(email: String, password: String) {
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, RecentChatsActivity::class.java)
                    startActivity(intent)
                } else {
                    Log.d(TAG, "Login:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
}

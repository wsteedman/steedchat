package com.steedles.steedchat.views

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.provider.MediaStore
import android.util.Log
import android.widget.PopupMenu
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.steedles.steedchat.R
import com.steedles.steedchat.RecentChatsActivity
import com.steedles.steedchat.helpers.*
import kotlinx.android.synthetic.main.activity_chat_messages.*
import java.util.*
import kotlin.collections.ArrayList

class ChatMessagesActivity : AppCompatActivity(), IUploadPhoto {

    private val db : Database = Database()
    private val TAG = "ChatMessages"

    private var messages: MutableList<ChatMessage> = arrayListOf()
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private var userId: String = ""
    private var receiverId: String = ""

    private lateinit var photoFragment: DialogPhotoFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_messages)

        val recId = intent.getStringExtra("chatId")
        if (auth.currentUser == null || recId?.length!! < 0) {
            goToChatHome()
            return
        }

        receiverId = recId
        userId = auth.currentUser!!.uid

        db.loadUserTrack(receiverId).get().addOnSuccessListener { document ->
            val chatUser = document.toObject(User::class.java)

            supportActionBar?.title = chatUser?.displayName
        }

        val chatId = db.getChatId(receiverId, userId)

        button_add_attachment.setOnClickListener {
            val popupMenu = PopupMenu(this, it)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menu_upload_image -> {
                        val fragManager = supportFragmentManager
                        photoFragment = DialogPhotoFragment()
                        photoFragment.show(fragManager, "PhotoFragment_tag")
                        true
                    }
                    else -> false
                }
            }

            popupMenu.inflate(R.menu.menu_attach)
            popupMenu.show()
        }

        db.loadChatMessages(chatId).addSnapshotListener { snapshot, e ->
            if (e != null) return@addSnapshotListener

            messages = snapshot?.toObjects(ChatMessage::class.java) as MutableList<ChatMessage>
            messages.sortBy { it.messageDateTime }
            recyclerview_chat_messages.adapter = ChatMessagesAdapter(ArrayList(messages), this, userId)
            Log.d(TAG, messages.toString())
        }

        button_send.setOnClickListener {
            if (edittext_message.text.toString().isNotEmpty()) {
                db.sendMessage(edittext_message.text.toString(), userId, receiverId)
                edittext_message.text.clear()
                edittext_message.requestFocus()
            } else {
                Toast.makeText(baseContext, "Message Box Empty",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPhotoSelect(photoUri: Uri) {
        Log.d(TAG, "Data - $photoUri")
        photoFragment.dismiss()
        db.sendMessage(photoUri.toString(), userId, receiverId, MessageTypes.IMAGE)
    }

    override fun onCloseFragment() {
        photoFragment.dismiss()
    }

    private fun goToChatHome() {
        val intent = Intent(this, RecentChatsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}

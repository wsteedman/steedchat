package com.steedles.steedchat.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.steedles.steedchat.R
import com.steedles.steedchat.helpers.Database
import com.steedles.steedchat.helpers.NewChatAdapter
import com.steedles.steedchat.helpers.User
import kotlinx.android.synthetic.main.activity_new_chat.*

class NewChatActivity : AppCompatActivity(), NewChatAdapter.OnUserListener {

    private val TAG = "ChatList"

    private lateinit var db : Database
    private lateinit var auth: FirebaseAuth
    private lateinit var newChatUsers: List<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_chat)
        db = Database()
        auth = FirebaseAuth.getInstance()
        supportActionBar?.title = "Start new chat..."
        db.loadAllUsers().addOnSuccessListener { documents ->
            val objects = documents.toObjects(User::class.java)
            val currentUser = objects.find { it.id == auth.currentUser?.uid }
            val filterOut = currentUser?.chatList!!.map {
                it.replace("|","").replace(auth.currentUser?.uid!!,"")
            }.toMutableList()
            filterOut.add(auth.currentUser?.uid!!)
            newChatUsers = objects.filter { !filterOut.contains(it.id) }
            recyclerview_new_chat.adapter = NewChatAdapter(ArrayList(newChatUsers), this, this)
        }
    }

    override fun onUserClick(position: Int) {
        val user = newChatUsers[position]
        db.createChatIfNotExist(user.id!!)

        val intent = Intent(this, ChatMessagesActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("chatId", user.id!!)

        startActivity(intent)
        finish()
    }
}

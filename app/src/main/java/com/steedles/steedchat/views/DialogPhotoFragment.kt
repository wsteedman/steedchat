package com.steedles.steedchat.views

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.firebase.storage.FirebaseStorage
import com.steedles.steedchat.R
import kotlinx.android.synthetic.main.activity_chat_messages.*
import kotlinx.android.synthetic.main.fragment_dialog_photo.*
import kotlinx.android.synthetic.main.fragment_dialog_photo.view.*
import java.util.*


class DialogPhotoFragment : DialogFragment() {

    private var uploadPhotoInterface : IUploadPhoto? = null

    private val PERMISSION_CODE = 1000;
    private val IMAGE_CAPTURE_CODE = 1001
    private val IMAGE_UPLOAD_CODE = 1002
    private val TAG = "ImageUpload"

    private var selectedPhotoUri: Uri? = null
    private lateinit var rootView: View
    private var uploadedUrl: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_dialog_photo, container)
        this.dialog?.setTitle("Upload Image")

        rootView.button_from_gallery.setOnClickListener {
            selectFromGallery()
        }

        rootView.button_take_photo.setOnClickListener {
            selectCamera()
        }

        rootView.button_cancel_image.setOnClickListener {
            uploadPhotoInterface?.onCloseFragment()
        }

        rootView.button_select_photo.setOnClickListener {
            if (uploadedUrl != null) {
                uploadPhotoInterface?.onPhotoSelect(uploadedUrl!!)
            } else {
                Toast.makeText(context, "Please select an image...",Toast.LENGTH_SHORT).show()
            }
        }
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            uploadPhotoInterface = activity as IUploadPhoto
        } catch (e: ClassCastException) {
            Log.d("MyDialog", "Activity doesn't implement the IUploadPhoto interface")
        }
    }

    private fun selectFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_UPLOAD_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK || data == null) return

        if (requestCode == IMAGE_UPLOAD_CODE) selectedPhotoUri = data.data
        val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri)
        rootView.image_uploaded.setImageBitmap(bitmap)
        uploadImage(selectedPhotoUri)
    }

    private fun uploadImage(uri: Uri?) {
        if (uri == null) return
        val filename = UUID.randomUUID().toString()
        val reference = FirebaseStorage.getInstance().getReference("/images/$filename")

        reference.putFile(selectedPhotoUri!!).addOnSuccessListener {
            Log.d(TAG, "Successfully uploaded: ${it.metadata?.path}")
            reference.downloadUrl.addOnSuccessListener {
                uploadedUrl = Uri.parse(it.toString())
            }
        }
    }

    private fun selectCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (activity?.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                activity?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                requestPermissions(permission, PERMISSION_CODE)
            } else openCamera()
        } else openCamera()
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        selectedPhotoUri = activity?.contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedPhotoUri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openCamera()
                else
                    Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

interface IUploadPhoto {
    fun onPhotoSelect(photoUri: Uri)
    fun onCloseFragment()
}